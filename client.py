import socket
import json


class Connect:
    HOST = "127.0.0.1"
    PORT = 55888
    BUFFER = 255

    def __init__(self):
        self.run = True
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.user = User(self)

    def send(self, data):
        try:
            self.client_socket.send(str(data).encode('utf8'))
        except socket.error as e:
            print(e)

    def input_and_send(self, prompt):
        try:
            data = input(prompt)
            self.send(data)
            return data
        except socket.error as e:
            print(e)

    def receive(self):
        json_data = self.client_socket.recv(self.BUFFER).decode('utf8')
        data = json.loads(json_data)
        return data

    def send_receive(self):
        try:
            msg = self.receive()
            print(msg)
        except socket.error as e:
            print(e)
            self.stop()

    def stop(self):
        print("Connection closed")
        self.run = False

    def connection(self):
        try:
            self.client_socket.connect((self.HOST, self.PORT))
            print(self.receive())
            self.main()

        except ConnectionRefusedError:
            print("Connection failed. Try again")
            return

    def main(self):
        command_map = {
            "uptime": self.send_receive,
            "help": self.send_receive,
            "info": self.send_receive,
            "logout": self.send_receive,
            "login": self.user.login,
            "stop": self.stop,
            "adduser": self.user.adduser,
            "showuser": self.user.showuser,
            "deluser": self.user.deluser,
            "msg": self.user.msg,
            "msgbox": self.user.msgbox,
            "delmsg": self.user.delete_msg
        }

        while self.run:
            command = input("Input command: ")
            self.send(command)
            handler = command_map.get(command, self.user.unknown_command)
            handler()
        self.client_socket.close()


class User:
    def __init__(self, connection):
        self.connection = connection

    def login(self):
        logged_in = bool(self.connection.receive())
        if logged_in:
            login = self.connection.input_and_send("Login: ")
            self.connection.input_and_send("Password: ")
            msg = self.connection.receive()
            response = self.available_command_msg(msg, login)
            self.connection.send(response)
        else:
            print("Already logged in")

    def available_command_msg(self, msg, login):
        if msg:
            print(f"You are logged in as '{login}'")
            response = f"'{login}' logged in"
            if login == "admin":
                print(
                    "Available commands: adduser, showuser, deluser, msg, msgbox, delmsg, logout, info, uptime, help, stop")
            else:
                print("Available commands: msg, msgbox, delmsg, logout, info, uptime, help, stop")
            return response
        else:
            response = "Login failed"
            print(response)
            return response

    def adduser(self):
        if self.admin_permission():
            username = input("Enter user name: ")
            if not username:
                print("Username cannot be empty!")
                return
            password = input("Enter a user password: ")
            if not password:
                print("Password cannot be empty!")
                return
            self.connection.send(username)
            self.connection.send(password)
            print(self.connection.receive())
        else:
            print("You have not permission for this command")

    def showuser(self):
        if self.admin_permission():
            users = self.connection.receive()
            for user in users:
                print(f"*{user}*")
        else:
            print("You have not permission for this command")

    def deluser(self):
        if self.admin_permission():
            user_to_delete = self.connection.input_and_send("Enter user login to delete: ")
            print(f"*{user_to_delete}* has been deleted")
        else:
            print("You have not permission for this command")

    def msg(self):
        self.connection.input_and_send("Enter an user to send message: ")
        msg = input("Enter a message: ")
        while len(msg) > 255:
            print("Too many characters in message!")
            msg = input("Enter a message: ")
        self.connection.send(msg)
        print(self.connection.receive())

    def msgbox(self):
        msg_list = self.connection.receive()
        if msg_list:
            for msg in enumerate(msg_list, 1):
                print(f"{msg[0]}. {msg[1]}")
        else:
            print("Message box is empty")

    def delete_msg(self):
        print(self.connection.receive())

    @staticmethod
    def unknown_command():
        print("Wrong command!")

    def admin_permission(self):
        return bool(self.connection.receive())

Connect().connection()
