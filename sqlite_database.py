import sqlite3
import os
import hashlib

def decorator(func):
    def wrapper(self, *args, **kwargs):
        try:
            with self.connection:
                return func(self, *args, **kwargs)
        except sqlite3.Error as e:
            print(e)

    return wrapper


class DataBase:
    def __init__(self):
        self.connection = self.db_connect()
        self.cursor = self.connection.cursor()

    @staticmethod
    def db_connect():
        try:
            connection = sqlite3.connect("cd.db")
            return connection
        except (sqlite3.OperationalError, sqlite3.Error) as error:
            print(error)

    @decorator
    def select_user(self):
        query = f"SELECT username FROM accounts;"
        try:
            self.cursor.execute(query)
            result = self.cursor.fetchall()
            return result
        except sqlite3.Error as e:
            print(e)

    @decorator
    def insert_user(self, username, password):
        password_hash = self.secure_password(password)
        salt, key = password_hash[:16], password_hash[16:]
        try:
            query_account = 'INSERT INTO accounts (username) VALUES (?)'
            self.cursor.execute(query_account, (username,))
            query_password = f"""INSERT INTO passwords (id_account, password_hash, salt) VALUES (
            (SELECT id FROM accounts WHERE username=?), ?, ?)"""
            self.cursor.execute(query_password, (username, key, salt))
            return f"User '{username}' with password '{password}' has been added"

        except sqlite3.Error as error:
            return f"Adding user failed. Try again {error}"

    @decorator
    def deleting_user(self, username):
        query = f"DELETE FROM accounts WHERE username = ?;"
        try:
            self.cursor.execute(query, (username,))
        except sqlite3.Error as e:
            print(e)

    @decorator
    def user_verification(self, username, user_password):
        try:
            password_hash, key = self.password_decode(username, user_password)
            if password_hash.digest() == key:
                self.update_status_user(username, True)
                return True
            else:
                return False
        except TypeError:
            return False

    @decorator
    def update_status_user(self, username, is_active):
        query = f"UPDATE accounts SET active = ? WHERE username = ?;"
        try:
            self.cursor.execute(query, (is_active, username))
        except sqlite3.Error as e:
            print(e)

    def password_decode(self, username, user_password):
        query = f"SELECT * FROM passwords WHERE id_account=(SELECT id FROM accounts WHERE username=?);"
        try:
            self.cursor.execute(query, (username,))
            password = self.cursor.fetchone()
            key, salt = password[2:4]
            key = bytes(key)
            salt = bytes(salt)
            password_hash = hashlib.sha256(salt + user_password.encode())
            return password_hash, key
        except sqlite3.Error as e:
            print(e)

    @decorator
    def login_admin_db(self):
        query = f"SELECT active FROM accounts WHERE username = ?;"
        try:
            self.cursor.execute(query, ('admin',))
            result = self.cursor.fetchone()
            return result[0]
        except sqlite3.Error as e:
            print(e)

    @decorator
    def check_amount_of_msg(self, user):
        select_query = f"SELECT COUNT(*) FROM msgbox WHERE username = ?;"
        try:
            self.cursor.execute(select_query, (user,))
            count = self.cursor.fetchone()[0]
            return count < 5 or user == "admin"
        except sqlite3.Error as e:
            print(e)

    @decorator
    def exist_user(self, user):
        query = f"SELECT username FROM accounts WHERE username = ?;"
        try:
            self.cursor.execute(query, (user,))
            user = self.cursor.fetchone()
            if not user:
                return True
        except sqlite3.Error as e:
            print(e)

    @decorator
    def send_message(self, to_user, message):
        if self.exist_user(to_user):
            return "This user not exist"
        if self.check_amount_of_msg(to_user):
            query = f"INSERT INTO msgbox (username, msg) VALUES (?, ?)"
            try:
                self.cursor.execute(query, (to_user, message))
                return "Message has been sent"
            except sqlite3.Error as e:
                print(e)
        else:
            return "Message box is full"

    @decorator
    def message_box(self, user):
        query = f"SELECT msg FROM msgbox WHERE username = ?;"
        try:
            self.cursor.execute(query, (user,))
            result = self.cursor.fetchall()
            print(len(result))
            return result
        except sqlite3.Error as e:
            print(e)

    @decorator
    def clear_message_box(self, user):
        query = f"DELETE FROM msgbox WHERE username = ?;"
        try:
            self.cursor.execute(query, (user,))
        except sqlite3.Error as e:
            print(e)

    @staticmethod
    def secure_password(password):
        salt = os.urandom(16)
        hash_password = hashlib.sha256(salt + password.encode())
        return salt + hash_password.digest()




