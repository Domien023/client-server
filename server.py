import socket
from sqlite_database import DataBase
import json
import time


class Connect:
    HOST = "127.0.0.1"
    BUFFER = 255
    PORT = 55888
    DATE = "28.04.2024"
    VERSION = "0.1.0"

    def __init__(self):
        self.client_socket = None
        self.run = True
        self.SERVER_SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.address = None
        self.user = User(self)

    def send(self, msg):
        data_json = json.dumps(msg)
        self.client_socket.send(data_json.encode('utf8'))

    def receive(self):
        response = self.client_socket.recv(self.BUFFER).decode('utf8')
        return response

    def main(self):
        while self.run:
            self.client_socket, self.address = self.SERVER_SOCKET.accept()

            print(f"Connected with {self.address[0]} : {self.address[1]}")

            msg_server = """Welcome on server
Available commands: login, info, uptime, help, stop"""
            data_json = json.dumps(msg_server)
            self.client_socket.send(data_json.encode('utf8'))
            while self.run:
                command = self.receive()
                command_map = {
                    "help": lambda: self.send(
                        """uptime - returns server uptime,
info - returns the server version number and its create date,
help - returns list of command with description,
stop - stops both server and client"""
                    ),
                    "uptime": lambda: self.send(self.user.uptime()),
                    "info": lambda: self.send(f"version {self.VERSION}, date {self.DATE}"),
                    "stop": self.user.logout,
                    "login": self.user.login,
                    "adduser": self.user.adduser,
                    "showuser": self.user.show_user,
                    "deluser": self.user.delete_user,
                    "msg": self.user.msg,
                    "msgbox": self.user.msg_box,
                    "delmsg": self.user.delete_msg,
                    "logout": self.user.logout
                }
                handler = command_map.get(command, self.user.unknown_command)
                handler()

    def connection(self):
        self.SERVER_SOCKET.bind((self.HOST, self.PORT))
        self.SERVER_SOCKET.listen()
        self.main()
        self.SERVER_SOCKET.close()


class User:

    def __init__(self, connect_class):
        self.connect = connect_class
        self.active = None
        self.t1 = time.time()

    def send(self, msg):
        self.connect.send(msg)

    def receive(self):
        return self.connect.receive()

    def login(self):
        if self.active is None:
            self.send(True)
            login = self.receive()
            password = self.receive()
            msg = DataBase().user_verification(login, password)
            if msg:
                self.active = login
            self.send(msg)
            print(self.receive())
        else:
            self.send(False)

    def logout(self):
        DataBase().update_status_user(self.active, False)
        msg = f"User '{self.active}' was logged out"
        self.active = None
        self.send(msg)

    def adduser(self):
        if DataBase().login_admin_db():
            self.send(True)
            username = self.receive()
            password = self.receive()
            msg = DataBase().insert_user(username, password)
            self.send(msg)
        else:
            self.send(False)

    def show_user(self):
        if DataBase().login_admin_db():
            self.send(True)
            users = DataBase().select_user()
            self.send(users)
        else:
            self.send(False)

    def delete_user(self):
        if DataBase().login_admin_db():
            self.send(True)
            user_to_delete = self.receive()
            DataBase().deleting_user(user_to_delete)
        else:
            self.send(False)

    def msg(self):
        to_user = self.receive()
        msg_from_user = self.receive()
        msg = DataBase().send_message(to_user, msg_from_user)
        self.send(msg)

    def msg_box(self):
        msg = DataBase().message_box(self.active)
        self.send(msg)

    def delete_msg(self):
        DataBase().clear_message_box(self.active)
        self.send("Messages has been deleted")

    def uptime(self):
        t2 = time.time()
        lifetime = t2 - self.t1
        n = lifetime / 60
        n = int(n)
        second = lifetime - (n * 60)
        return f"Server uptime: {n} minutes {int(second)} second"

    def stop(self):
        if self.active is not None:
            DataBase().update_status_user(self.active, False)

    def unknown_command(self):
        msg = "Wrong command!"
        self.send(msg)


Connect().connection()
